var glpDirectives = angular.module("glp.directives");

glpDirectives.directive('glpInformation', function () {
    return {
        restrict: 'E',
        replace: true,
        template: '<span class="badge"><i class="fa fa-info"></i></span>',
        link: function (scope, element, attrs){
            $(element).popover({
                trigger: 'hover',
                html: true,
                content: attrs.infoText,
                placement: attrs.infoPlacement,
                container: 'body'
            });
        }
    };
});
