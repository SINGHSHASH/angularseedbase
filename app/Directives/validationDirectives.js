﻿var glpDirectives = angular.module("glp.directives");

glpDirectives.directive('glpValidateOnBlur', function () {
    return {
        restrict: 'A',
        require: '^form',
        link: function (scope, el, attrs, formCtrl) {
            angular.element(document).ready(function () {
                var inputName = attrs.glpValidateOnBlur;
                var inputEl = el[0].querySelector("input");
                var inputNgEl = angular.element(inputEl);

                inputNgEl.bind('blur', function () {
                    el.toggleClass('has-error', formCtrl[inputName].$invalid && !formCtrl[inputName].$pristine);
                });
            });
        }
    }
});

glpDirectives.directive('glpValidateOnSubmit', function () {
    return {
        restrict: 'A',
        require: '^form',
        link: function (scope, el, attrs, formCtrl) {
            var inputName = attrs.glpValidateOnSubmit;

            scope.$on('saveContract', function () {
                el.toggleClass('has-error', formCtrl[inputName].$invalid);
            });
        }
    }
});

glpDirectives.directive('glpMultiSelectRequired', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attr, ctrl) {
            var validator = function (value) {
                if (!value || !value.length) {
                    ctrl.$setValidity('required', false);
                    return [];
                } else {
                    ctrl.$setValidity('required', true);
                    return value;
                }
            };

            ctrl.$parsers.unshift(validator);
        }
    };
});

var compareDates = function (date1, date2) {
    return (new Date(date1.getFullYear(), date1.getMonth(), date1.getDate()) - new Date(date2.getFullYear(), date2.getMonth(), date2.getDate()));
};

glpDirectives.directive('glpValidateMinDate', function ($parse) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attr, ctrl) {
            var minDate = scope[attr.minDate];

            var validator = function (valiDate) {
                ctrl.$setValidity('minDate', true);
                if (angular.isDate(minDate) && angular.isDate(valiDate) && compareDates(valiDate, minDate) < 0) {
                    ctrl.$setValidity('minDate', false);
                }
                return valiDate;
            };

            ctrl.$parsers.push(validator);

            scope.$watch($parse(attr.minDate), function (value) {
                minDate = value;
                validator(ctrl.$modelValue);
            });
        }
    };
});

glpDirectives.directive('glpValidateMaxDate', function ($parse) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attr, ctrl) {
            var maxDate = scope[attr.maxDate];

            var validator = function (valiDate) {
                ctrl.$setValidity('maxDate', true);
                if (angular.isDate(maxDate) && angular.isDate(valiDate) && compareDates(valiDate, maxDate) > 0) {
                    ctrl.$setValidity('maxDate', false);
                }
                return valiDate;
            };

            ctrl.$parsers.push(validator);

            scope.$watch($parse(attr.maxDate), function (value) {
                maxDate = value;
                validator(ctrl.$modelValue);
            });

        }
    };
});

glpDirectives.directive('glpDisableChildElements', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            var originalClickEvent = element.click;

            var disableElements = function (elementsToDisable, disabled) {
                if (disabled) {
                    elementsToDisable.attr('disabled', true);
                    elementsToDisable.addClass('disabled');
                    elementsToDisable.bind("click", function (event) {
                        if (event.returnValue) {
                            event.returnValue = false;
                        }
                        if (event.preventDefault) {
                            event.preventDefault();
                        }
                        if (event.stopPropagation) {
                            event.stopPropagation();
                        }
                        return false;
                    });
                } else {
                    elementsToDisable.attr('disabled', false);
                    elementsToDisable.removeClass('disabled');
                    elementsToDisable.bind("click", originalClickEvent);
                }
            };

            scope.$watch(attr.glpDisableChildElements, function (value) {
                disableElements(element, value);
                disableElements(element.find('*'), value);
            });

            if (scope.$eval(attr.glpDisableChildElements)) {
                disableElements(element, true);
                disableElements(element.find('*'), true);
            }
        }
    };
});

glpDirectives.directive('glpCompareLt', function ($parse) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function(scope, element, attr, ctrl){
            var compareTo = null;
            var errorName = null;
            var validator = function (value) {
                ctrl.$setValidity('compareLt', true);
                if (attr.glpErrorName != undefined) {
                    errorName = attr.glpErrorName;
                }
                var number1 = value;
                var number2 = -1;
                if (compareTo != undefined) {
                    number2 = parseInt(compareTo);
                }
                var result = number1 < number2;
                ctrl.$setValidity(errorName, result);
                return value;
            };

            ctrl.$parsers.push(validator);

            scope.$watch($parse(attr.glpCompareLt), function (value){
                compareTo = value;
                validator(ctrl.$modelValue);
            });
        }
    }
});

glpDirectives.directive('glpCompareLe', function ($parse) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attr, ctrl) {
            var compareTo = null;
            var errorName = null;
            var validator = function (value) {
                ctrl.$setValidity('compareLe', true);
                if (attr.glpErrorName != undefined) {
                    errorName = attr.glpErrorName;
                }
                var number1 = value;
                var number2 = -1;
                if (compareTo != undefined) {
                    number2 = parseInt(compareTo);
                }
                var result = number1 <= number2;
                ctrl.$setValidity(errorName, result);
                return value;
            };

            ctrl.$parsers.push(validator);

            scope.$watch($parse(attr.glpCompareLe), function (value) {
                compareTo = value;
                validator(ctrl.$modelValue);
            });
        }
    }
});