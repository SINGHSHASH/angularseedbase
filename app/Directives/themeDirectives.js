﻿var glpDirectives = angular.module("glp.directives");

glpDirectives.directive("glpThemeSwitcherMenu", [
    "cookieService", "$compile", "$http", function(cookieService, $compile, $http){

        var tcTheme = { name: "Thomas Cook", cssCdn: "/Content/bootstrap.thomascook.css" };
        return {
            restrict: 'A',
            link: function(scope, element, attributes){
                function populateElement(){
                    element.find("li").remove();

                    for (var i = 0; i < scope.themes.length; i++){
                        var currentTheme = scope.themes[i];
                        element.append($compile("<li><a ng-click='setTheme(\"" + currentTheme.name + "\")' class='btn' role='button'>" + currentTheme.name + "</a></li>")(scope));
                    }
                }

                function getTheme(name){
                    var result = tcTheme;

                    for (var i = 0; i < scope.themes.length; i++){
                        if (scope.themes[i].name == name){
                            result = scope.themes[i];
                        }
                    }

                    return result;
                }

                var tagName = element.prop("tagName");
                if (tagName != "UL" && tagName != "OL"){
                    throw ("Theme Switcher must be attached to a list");
                }

                if (!scope.setTheme){
                    scope.setTheme = function(name){
                        var stylesheetReference = angular.element("#" + attributes.glpThemeSwitcherMenu);
                        if (stylesheetReference){
                            var theme = getTheme(name);
                            if (theme){
                                stylesheetReference.attr("href", theme.cssCdn);
                                cookieService.set("glp.theme", { name: theme.name, cssCdn: theme.cssCdn }, { expires: 30, expirationUnit: 'days' });
                            }
                        }
                    };
                }

                $http.get("http://api.bootswatch.com/3/").then(function(result){
                    scope.themes = [tcTheme].concat(result.data.themes);
                    populateElement();
                });
            }
        };
    }
]);

glpDirectives.directive("glpThemeSwitcherRestore", [
    "cookieService", function(cookieService){
        return {
            restrict: 'A',
            link: function(scope, element, attributes){
                var theme = cookieService.get("glp.theme");
                if (theme){
                    var stylesheetReference = angular.element("#" + attributes.glpThemeSwitcherRestore);
                    if (stylesheetReference){
                        stylesheetReference.attr("href", theme.cssCdn);
                        cookieService.set("glp.theme", theme, { expires: 30, expirationUnit: 'days' });
                    };
                }
            }
        };
    }
]);