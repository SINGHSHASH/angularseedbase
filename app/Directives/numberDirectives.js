var glpDirectives = angular.module("glp.directives");
glpDirectives.directive("glpPosInt", function () {
return {
        restrict: 'A',
        require: 'ngModel',
        scope: {
            ngModel: '='
        },
        link: function(scope){
            scope.$watch('ngModel', function(newValue){
                if (isNaN(newValue)||newValue==null){
                    scope.ngModel = 0;
                }
                else{
                    var result = parseInt(newValue, 10);
                    scope.ngModel = result;
                }
            });
        }
    }
});

glpDirectives.directive("glpPosNum", function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        scope: {
            ngModel: '='
            , maxDecimalPlaces: '@'
        },
        link: function (scope) {
            scope.$watch('ngModel', function (newValue) {
                if (isNaN(newValue) || newValue == null) {
                    scope.ngModel = 0;
                }
                else {
                    var result;
                    if (scope.maxDecimalPlaces != undefined && scope.maxDecimalPlaces.length > 0) {
                        var decimalPlaces = parseInt(scope.maxDecimalPlaces, 10);
                        result = parseFloat(newValue);
                        result = Math.floor(result * Math.pow(10,decimalPlaces)) / Math.pow(10,decimalPlaces);
                        result = parseFloat(result);
                        scope.ngModel = result;
                    }
                    else {
                        result = parseFloat(newValue).toFixed(0);
                        result = parseFloat(result);
                        scope.ngModel = result;
                    }
                }
            });
        }
    }
});