var glpDirectives = angular.module("glp.directives");

glpDirectives.directive('glpDatePicker', [
    '$compile', function ($compile) {
        return {
            restrict: 'E',
            require: '?ngModel',
            replace: true,
            scope: {
                buttonText: '@',
                minDate: '=',
                maxDate: '='
            },
            link: function (scope, element, attr, ngModel) {
                if (!ngModel) return;

                var inputNode = angular.element(element.find('input')[0]);

                var optionsObj = {};
                optionsObj.dateFormat = 'dd/mm/yy';
                optionsObj.changeMonth = true;
                optionsObj.changeYear = true;
                optionsObj.showMonthAfterYear = true;
                optionsObj.minDate = scope.minDate;
                optionsObj.maxDate = scope.maxDate;
                optionsObj.showAnim = '';

                var updateModel = function (dateTxt) {
                    if (dateTxt && !angular.isDate(dateTxt)) {
                        var dateParts = dateTxt.split('/');
                        if (dateParts.length == 3) {
                            dateTxt = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
                        }
                    }
                    scope.$apply(function () {
                        ngModel.$setViewValue(dateTxt);
                    });
                };

                var rangeCheck = function(){
                    var viewValue = ngModel.$viewValue;
                    if (viewValue === "") {
                        return;
                    }
                    if (viewValue < scope.minDate) {
                        ngModel.$setViewValue(scope.minDate);
                    }
                    if (viewValue > scope.maxDate){
                        ngModel.$setViewValue(scope.maxDate);
                    }
                }

                optionsObj.onSelect = function (dateTxt) {
                    var selectedDate = inputNode.datepicker('getDate');
                    updateModel(selectedDate);
                    if (inputNode.blur) { inputNode.blur(); }
                };

                ngModel.$render = function () {
                    inputNode.datepicker('setDate', ngModel.$viewValue || '');
                };
                inputNode.datepicker(optionsObj);
                inputNode.inputmask("date", {
                    "oncomplete": function () {
                        updateModel(inputNode.val());
                    },
                    "onincomplete": function () {
                        updateModel('');
                    }
                });

                scope.$watch('minDate', function (newValue, oldValue) {
                    if (newValue != oldValue) {
                        inputNode.datepicker("option", "minDate", newValue);
                    }
                    rangeCheck();
                });

                scope.$watch('maxDate', function (newValue, oldValue) {
                    if (newValue != oldValue) {
                        inputNode.datepicker("option", "maxDate", newValue);
                    }
                    rangeCheck();
                });

                scope.openCal = function () {
                    inputNode.datepicker('show');
                }

            },
            template: '<span><span class="input-group" > ' +
                '<input type="text" ' +
                'class="form-control" ' +
                'placeholder="dd/mm/yyyy"/> ' +
                '<span class="input-group-btn"> ' +
                '<button type="button" class="btn btn-default" ' +
                'ng-class="{\'btn-with-text\': buttonText!=null}" ng-click="openCal()"> ' +
                '<span ng-show=\'buttonText!=null\' class="clearfix">{{buttonText}}</span> ' +
                '<i class="glyphicon glyphicon-calendar"></i>' +
                '</button>' +
                '</span>' +
                '</span></span>'
        }
    }
]);