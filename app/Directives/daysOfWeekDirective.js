var glpDirectives = angular.module("glp.directives");
/*notes for dev
1: will restrict to element
2: will require the dataList attribute
3: the datalist will be of the format [{day:1, isActive:true},{day:2, isActive:false}] day 1 = sun ..2 = sat
4: the template will be a table with a single row and 8 cols 1st col will be a select all the next seven day of week mon header
5: loop the data given and populate the scope accordnly
6: the select all allows selection of all to true nothing else
7: if oe day is set false then select all set to false - that has no real scope meaning just a quick process
8: ensure controller is kept in sync with changes and refelects when saved and when refreshed
*/
glpDirectives.directive("glpDaysOfWeek", function(){
    return {
        restrict: 'E',
        scope: {
            days: "=",
            daysOfWeekDefaults: "@"
        },
        template: '<div>'
            + '<div>'
            + '<table ng-disabled="!isValid">'
            + '<thead>'
            + '<tr>'
            + '<th>All</th>'
            + '<th>Su</th>'
            + '<th>Mo</th>'
            + '<th>Tu</th>'
            + '<th>We</th>'
            + '<th>Th</th>'
            + '<th>Fr</th>'
            + '<th>Sa</th>'
            + '</tr>'
            + '</thead>'
            + '<tbody>'
            + '<tr>'
            + '<td><input id="allCheck" type="checkbox" ng-click="setAllToActiveOrInactive($event)"></td>'
            + '<td ng-repeat="item in days| orderBy:item.day"> <input type="checkbox" ng-model="item.isActive" ng-disabled="isCheckboxDisabled" ng-click="isAllToBeChecked($event,item.day)"></td>'
            + '</tr>'
            + '</tbody>'
            + '</table>'
            + '</div>'
            + '</div>',
        replace: true,
        link: function(scope, element, attr){
            scope.currentElement = element;
            var attrIsDaysOfWeek = attr.isDaysOfWeekNew;
            scope.isDayOfWeekNew = attrIsDaysOfWeek === undefined ? false : scope.$parent[attrIsDaysOfWeek] === undefined ? false : scope.$parent[attrIsDaysOfWeek];
            scope.setAllToActiveOrInactive = function($event){
                if ($event != undefined && $event.target != undefined){
                    var checkBox = $event.target;
                    if (checkBox != undefined){
                        var action = (checkBox.checked ? 'add' : 'remove');
                        if (action === 'add'){
                            //1: update all items
                            scope.days.forEach(function(day){
                                day.isActive = true;
                            });
                            //2: disable individual entry on days
                            scope.isCheckboxDisabled = true;
                        }
                        else{
                            //allow enable on all days
                            scope.isCheckboxDisabled = false;
                        }
                    }
                }
                else{
                    scope.isCheckboxDisabled = false;
                }
            };
            scope.isAllToBeChecked = function($event, dayOfWeek){
                //1: lets check to see if we should enable the all tick box
                var currCheckBox = null;
                var currDay = null;
                if ($event){
                    currCheckBox = $event.target;
                    var action = (currCheckBox.checked ? 'add' : 'remove');
                }
                if (dayOfWeek){
                    currDay = dayOfWeek;
                }
                var setAllChecked = true;
                scope.days.forEach(function(day){
                    if (setAllChecked){
                        if ($event && currDay){
                            if (day.day == currDay){
                                if (action === 'remove'){
                                    setAllChecked = false;
                                }
                            }
                            else{
                                if (day.isActive === false){
                                    setAllChecked = false;
                                }
                            }
                        }
                        else{
                            if (day.isActive === false){
                                setAllChecked = false;
                            }
                        }
                    }
                });
                if (setAllChecked){

                    var checkBox = scope.currentElement.find("#allCheck")[0];
                    if (checkBox){
                        checkBox.checked = true;
                    }
                    scope.isCheckboxDisabled = true;
                }
            };
            scope.isCheckboxDisabled = false;
            //if we have incorrect number of items then disable output
            scope.isValid = (angular.isArray(scope.days) && scope.days.length == 7) ? true : false;

            if (scope.isValid == true){
                var defaultsArray = JSON.parse('[' + (scope.daysOfWeekDefaults != undefined
                    ? scope.daysOfWeekDefaults
                    : '') + ']');

                scope.isSetDefaults = scope.isDayOfWeekNew && angular.isArray(defaultsArray);

                if (scope.isSetDefaults){

                    defaultsArray.forEach(function(dayToSet){
                        switch (dayToSet){
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                            case 5:
                            case 6:
                            case 7:
                                scope.days[dayToSet - 1].isActive = true;
                                break;
                            case 0:
                                scope.days.forEach(function(day){
                                    day.isActive = true;
                                });
                                break;
                        }
                    });
                }
                scope.isAllToBeChecked();
            }
            else{
                throw ('Invalid array to parse. Array must contain 7 items');
            }
        }
    };
});