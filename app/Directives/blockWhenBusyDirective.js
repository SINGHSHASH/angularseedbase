var glpDirectives = angular.module("glp.directives");

glpDirectives.directive('glpBlockWhenBusy', function () {

    var overlayElement, spinnerElement, parent;

    function show() {
        overlayElement.css('display', 'inline-block');
        syncSize();
    }

    function syncSize() {
        var height = parent.height();
        overlayElement.css('height', height);
        overlayElement.css('width', parent.width() - parent.position().left);
        overlayElement.css('top', parent.position().top);
    }

    function hide() {
        overlayElement.css('display', 'none');
    }

    return {
        restrict: 'A',
        link: function (scope, element, attributes) {
            parent = element;
            var spinnerClass = attributes.glpBlockWhenBusy;
            var model = attributes.bwbModel;
            var url = attributes.bwbUrl;

            if (!model && !url) {
                throw ('Block when directive requires model or url to be specified e.g. glp-block-when-busy data-bwb-model="isBusy" or glp-block-when-busy data-bwb-url="/Suppliers"');
            }

            if (model && url) {
                throw ('Block when directive does not support both url and model mode simultaneously, remove either data-bwb-model or data-bwb-url');
            }

            overlayElement = angular.element("<div style='background-color: rgba(71, 73, 73, 0.1); display: none; position: absolute; text-align: center; overflow: hidden; clear: both;'></div>");
            spinnerElement = angular.element("<i class='fa fa-spinner fa-spin' style='position: absolute; left: 50%; top: 50%;'></i>");
            if (spinnerClass){
                spinnerElement.addClass(spinnerClass);
            }
            overlayElement.append(spinnerElement);
            parent.append(overlayElement);

            if (model) {
                if (scope[model] === undefined) {
                    scope[model] = false;
                }

                scope.$watch(model, function () {
                    var value = scope[model];
                    if (value == true) {
                        show();
                    }
                    else {
                        hide();
                    }
                });
            }
            else {
                scope.$on('ajaxRequestStarted', function (event, args) {
                    if (args == url) {
                        show();
                    }
                });
                scope.$on('ajaxRequestCompleted', function (event, args) {
                    if (args == url) {
                        hide();
                    }
                });
            }
        }
    };
}
);