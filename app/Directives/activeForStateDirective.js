var glpDirectives = angular.module("glp.directives");

glpDirectives.directive('glpActiveForState', ['$state', function($state){

        return {
            restrict: 'A',
            link: function(scope, element, attributes){
                var activeClass = attributes.afsClass || 'active';
                var activeState = attributes.glpActiveForState;

                if (!activeState){
                    throw ('Active for state directive requires a state to be specified e.g. glp-active-for-state="State"');
                }

                scope.$on('$stateChangeSuccess', function(){
                    var newState = $state.current.name;
                    if (newState.indexOf(activeState) !== -1){
                        element.addClass(activeClass);
                    }
                    else{
                        element.removeClass(activeClass);
                    }
                });
            }
        };
    }
]);