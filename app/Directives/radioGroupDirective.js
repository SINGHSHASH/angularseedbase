/*
dev thoughts
researchins and utilising bootstrap there are two thoughts 
1: use plain radio
2: use btn as a radio group
will create the btn radio group initially as will match in with the active process not the same but...
will then create a plain old radio group - that way team can switch over as bootstrap will control the display
again the mgmodel is the key and that will be an id that will need to match to the id of the options pattern
also pass in the option list and the process will requie a pattern like [{id:int, description:string},{id:int, description:string}...]
also pass in the current active id
also pass in a bootstrap layout
limit to element

*/
var glpDirectives = angular.module("glp.directives");
glpDirectives.directive("glpRadioGroup", function () {
    return {
        restrict: 'E',
        require: 'ngModel',
        scope: {
            ngModel: "=",
            options: "=",
            boot: "@",
            key: "@",
            description: "@",
            radioGroupOnChange: "&",
            radioGroupIsHidden: "=",
            radioGroupIsHiddenFunction: "&",
            radioGroupIsDisabled: "&"
        },
        template: '<button type="button" class="btn-sm btn-{{boot}}" id="{{createId(option)}}" ' +
            'ng-class="{active: isActive(option)}" ' +
            'ng-repeat="option in options" ' +
            'ng-click="setActive(option,$event)" ' +
            'ng-disabled="radioGroupIsDisabled(option)" ' +
            'ng-hide="{{setHide(option)}}">' +
            '{{getOptionDescription(option)}}' +
            '</button>',
        link: function (scope, element, attr, ctrl) {
            var attrRadioGroupOnChange = scope.radioGroupOnChange();
            var attrRadioGroupIsHidden = scope.radioGroupIsHidden == undefined ? false : scope.radioGroupIsHidden;
            var attrRadioGroupIsHiddenFunction = scope.radioGroupIsHiddenFunction();

            scope.setHide = function (option) {
                if (attrRadioGroupIsHidden ==true) {
                    if (attrRadioGroupIsHiddenFunction != undefined) {
                        return attrRadioGroupIsHiddenFunction(option[scope.key]);
                    }
                    else {
                        return false;
                    }
                }
                else{
                    return false;
                }
            }

            scope.setActive = function (option, $event) {
                scope.ngModel = option[scope.key];
                if ($event.stopPropagation) {
                    $event.stopPropagation();
                }
                if ($event.preventDefault) {
                    $event.preventDefault();
                }
                $event.cancelBubble = true;
                $event.returnValue = false;
                if (attrRadioGroupOnChange != undefined) {
                    attrRadioGroupOnChange(option[scope.key]);
                }
            }
            scope.isActive = function (option) {
                //for each option in the array determine if its id is the active one.
                return option[scope.key] == scope.ngModel;
            }
            scope.createId = function (option) {
                return option[scope.description] + '-' + option[scope.key];
            }
            scope.getOptionDescription = function (option) {
                return option[scope.description];
            }
            scope.isValid = (angular.isArray(scope.options)) ? true : false;
            if (scope.isValid == true) {
                scope.options.forEach(function (option) {
                    if (scope.isValid) {
                        scope.isValid = scope.key in option && scope.description in option;
                    }
                });

            }
            if (!scope.isValid) {
                throw ('Invalid array to parse. Array must have at least one key and a description.');
            }
        }
    }
});

/*NOTE: the std radio group has not all the above implemented please bear this in mind */
glpDirectives.directive("glpRadioGroupStd", function () {
    return {
        restrict: 'E',
        require: 'ngModel',
        scope: {
            ngModel: "=",
            options: "=",
            key: "@",
            description: "@"
        },
        template: '<span ng-repeat="option in options"> ' +
                  '<span><label class="radioGroupLabel"> ' +
                  '<input type="radio" ' +
                  'value="{{getValue(option)}}" ' +
                  'name="radio" ' +
                  '</label>{{getOptionDescription(option)}}</span>' +
                  '</span>',
        link: function (scope, element, attr, ctrl) {
            scope.setActive = function (option, $event) {
                scope.ngModel = option[scope.key];
                if ($event.stopPropagation) {
                    $event.stopPropagation();
                }
                if ($event.preventDefault) {
                    $event.preventDefault();
                }
                $event.cancelBubble = true;
                $event.returnValue = false;
            }
            scope.isActive = function (option) {
                //for each option in the array determine if its id is the active one.
                return option[scope.key] == scope.ngModel;
            }
            scope.getValue = function (option) {
                return option[scope.key];
            }
            scope.getOptionDescription = function (option) {
                return option[scope.description];
            }
            scope.isValid = (angular.isArray(scope.options)) ? true : false;
            if (scope.isValid == true) {
                scope.options.forEach(function (option) {
                    if (scope.isValid) {
                        scope.isValid = scope.key in option && scope.description in option;
                    }
                });
            }
            if (!scope.isValid) {
                throw ('Invalid array to parse. Array must have at least one key and a description.');
            }
        }
    }
});
