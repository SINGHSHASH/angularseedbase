var glpDirectives = angular.module("glp.directives");

glpDirectives.directive('glpNotificationPresenter', [
    'notificationService', function(notificationService){

        var parent;
        var notificationElements = [];

        function addNotification(notification, orientation){
            var notificationElement = angular.element("<div id='notification" + notification.id + "' class='alert " + notification.uiHint + "' style='padding: 10px; margin: 2px'>" + notification.message + "</div>");
            if (orientation && orientation === 'horizontal'){
                notificationElement.css('float', 'left');
            }
            notificationElements.push({ id: notification.id, element: notificationElement });
            parent.insertBefore(notificationElement[0], parent.firstChild);
        }

        function deleteNotification(notification){
            
            var notificationElementId = '#notification' + notification.id;
            angular.element(parent).find(notificationElementId).remove();

            for (var i = 0; i < notificationElements.length; i++){
                if (notificationElements[i].id === notification.id){
                    notificationElements.splice(i, 1);
                    break;
                }
            }
        }

        function updateNotification(notification){
            for (var i = 0; i < notificationElements.length; i++){
                if (notificationElements[i].id === notification.id){
                    var element = notificationElements[i].element;
                    element.text(notification.message);
                    element.removeClass('alert-info').removeClass('alert-danger').removeClass('alert-success').removeClass('alert-warning').addClass(notification.uiHint);
                    break;
                }
            }
        }

        return {
            restrict: 'A',
            link: function(scope, element, attributes){

                var tagName = element.prop("tagName");
                if (tagName != "DIV"){
                    throw ("Notification Presenter must be attached to a div");
                }

                element.html('');
                parent = element[0];

                var styles = {
                    position: 'fixed'
                };

                var orientation = attributes.dpOrientation || 'vertical';
                var vPosition = attributes.dpVposition || 'bottom';
                var hPosition = attributes.dpHposition || 'left';

                if (vPosition === 'top'){
                    styles.top = '0';
                    styles.marginBottom = 'auto';
                }
                else{
                    styles.bottom = '0';
                    styles.marginTop = 'auto';
                }

                if (orientation === 'vertical'){
                    styles.width = '225px';
                    styles.maxHeight = '250px';

                    if (hPosition === 'right'){
                        styles.right = '0';
                        styles.marginLeft = 'auto';
                    }
                    else{
                        styles.left = '0';
                        styles.marginRight = 'auto';
                    }
                }

                if (orientation === 'horizontal'){
                    styles.right = '0px';
                    styles.left = '0px';
                }

                element.css(styles);

                for (var i = 0; i < notificationService.notifications.length; i++){
                    addNotification(notificationService.notifications[i], orientation);
                }

                notificationService.subscribe(function(notification){
                    if (notification.state === 0){
                        addNotification(notification, orientation);
                        return;
                    }

                    if (notification.state === -1){
                        deleteNotification(notification);
                        return;
                    }

                    updateNotification(notification);
                });

                notificationService.init();
            }
        };
    }
]);